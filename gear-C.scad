include </Users/robertoprevidi/Dropbox/reprap/openscad_lib/parametric_involute_gear_v5.0.scad>;
module original_gear() {
  gear (
	number_of_teeth = 15,
	circular_pitch=300,
	gear_thickness = 12,
	rim_thickness = 15,
	rim_width = 5,
	hub_thickness = 22,
	hub_diameter=30,
	bore_diameter=13,
	circles=9);
}

module screw_holes()
{
  translate([9,0,34]) 
    cube([2.2,5.5,40],center=true);
  translate([-9,0,34]) 
    cube([2.2,5.5,40],center=true);
 translate([0,0,17])
    rotate([0,90,0])
    cylinder(r=1.5,h=50,center=true);
}
module final_gear()
{
  difference(){
    import_stl("gear-C_deletable.stl");
    screw_holes();
//    rotate([0,0,90]) screw_holes();
  }
}
// MAKE_TARGETS_DISABLE_CONCURRENCY
// TARGETS: ##deletable ##final
original_gear(); // ##deletable
final_gear(); // ##final