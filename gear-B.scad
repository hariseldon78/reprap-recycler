include </Users/robertoprevidi/Dropbox/reprap/openscad_lib/parametric_involute_gear_v5.0.scad>;
module large() {
  gear (
      number_of_teeth = 40,
      circular_pitch=300,
      gear_thickness = 14,
      rim_thickness = 14,
      rim_width = 5,
      hub_thickness = 0,
      hub_diameter=0,
      bore_diameter=7.9,
      circles=0);
}

module small() {
  gear (
    	number_of_teeth = 15,
    	circular_pitch=300,
    	gear_thickness = 14,
    	rim_thickness = 14,
    	rim_width = 5,
    	hub_thickness = 0,
    	hub_diameter=0,
    	bore_diameter=7.9,
    	circles=0);
}

module both() {
  large();
  translate([0,0,13]) small();
}

// TARGETS: ##small ##large ##together
large(); // ##large
small(); // ##small
both(); // ##together