T=0.5; //holes half tolerance

module cub_linder(r,h)
{
	translate([0,0,h/2]) cube([r*2,r*2,h],center=true);
}

module test() {
	rotate([20]) cylinder(r=10,h=20);
	translate([0,0,20]) rotate([20]) cub_linder(r=10,h=20);
}

module structure_holes()
{
	for(x=[-18,+18])
		for(y=[-6,+6])
			translate([x,y,0]) cylinder(r=4,h=20,center=true);
}

module _base_module(width,l,angle,att)
{
	width=max(width,10);
	difference() {
		cube([l,width,14],center=true);
		translate([-32,0,0]) rotate([90,0,0]) cylinder(r=4+T,h=width*2,center=true);
		translate([32,0,0]) rotate([90,0,0]) cylinder(r=4+T,h=width*2,center=true);
		if (att)
		{
			translate([-40,0,0]) rotate([90-angle,0,0]) cylinder(r=4+T,h=width*2,center=true);
			translate([40,0,0]) rotate([90-angle,0,0]) cylinder(r=4+T,h=width*2,center=true);
		}
	}
}

module base_module(width) {
	_base_module(width,78,0,false);
}

module base_attachment_module(width,angle) {
	_base_module(width,94,angle,true);
}
module bearing22_90() {
	difference() {
		base_attachment_module(width=28,angle=90);
  		translate([0,0,4]) cylinder(r=11+T,h=14,center=true);
		cylinder(r=8,h=20,center=true);

		structure_holes();
	}
}


module bearing19_90() {
	difference() {
		base_attachment_module(width=25,angle=90);
  		translate([0,0,5]) cylinder(r=9.5+T,h=14,center=true);
//  		translate([0,0,4]) cylinder(r=6.5+T,h=14,center=true);
		cylinder(r=7.7,h=20,center=true);

		structure_holes();
	}
}

module end_holder_90() {
	difference() {
		base_attachment_module(width=27,angle=90);
		cylinder(r=6+T,h=20,center=true);
		rotate([90,0,0]) cylinder(r=1.5,h=40,center=true);
		for(y=[-9.5,9.5])		
			translate([0,y,5.5]) cube([5.5+T,2.3+T,14],center=true);
		structure_holes();
	}
}

module _motor_holder_90(w,rot,hole)
{
	difference() {
		base_attachment_module(width=w,angle=90);
		translate([0,0,3.5]) rotate([0,0,rot]) cube([42+T,42+T,14],center=true);
		cylinder(r=hole/2+T,h=30,center=true);
		rotate([0,0,rot])
			for(x=[-16,16])
				for(y=[-16,16])
					translate([x,y,0]) cylinder(r=1.5,h=30,center=true);
	}
	
}
module motor_holder_slim_90() {
	_motor_holder_90(w=20,rot=30,hole=13);
}


module motor_holder_90() {
	_motor_holder_90(w=52,rot=0,hole=32);
}

module motor_holder_half_90() {
	difference() {
		_motor_holder_90(w=52,rot=0,hole=13);
		translate([0,20,0]) cube([100,40,40],center=true);
	}
}

module bars_joint(angle) {
	difference() {
		intersection() {
//			cube([23,23,23],center=true);
//			rotate([angle,0,0]) cube([23,23,23],center=true);

		translate([-4-T,0,0]) cylinder(r=20,h=10,center=true);
		translate([4+T,0,0]) rotate([angle,0,0]) cylinder(r=20,h=10,center=true);


		}
		//sphere(r=11.5,center=true);
		translate([-4-T,0,0]) cylinder(r=4+T,h=50,center=true);
		translate([4+T,0,0]) rotate([angle,0,0]) cylinder(r=4+T,h=50,center=true);

	}
}

module material_feeder() {
	difference() {
		union() {
			base_attachment_module(width=27,angle=90);
			translate([0,0,6]) cub_linder(r=10,h=22);
			translate([0,0,6]) rotate([45,0,0]) cub_linder(r=7,h=40);
		}
		translate([0,0,6]) rotate([45,0,0]) cylinder(r=5,h=40);
		translate([0,0,48]) cube([80,80,40],center=true);
		cylinder(r=6+T,h=14,center=true);
		translate([0,0,7]) cylinder(r=4.8+T,h=50);
		rotate([90,0,0]) cylinder(r=1.5,h=40,center=true);
		for(y=[-9.5,9.5])		
			translate([0,y,-5.5]) cube([5.5+T,2.3+T,14],center=true);
		structure_holes();
	}
}
//TARGETS: ##bearing19 ##bearing22 ##jointA ##jointB ##jointC ##jointD ##end ##motor_slim ##motor_half ##feeder
bearing19_90(); //##bearing19
bearing22_90(); //##bearing22
bars_joint(angle=90); //##jointA
bars_joint(angle=60); //##jointB
bars_joint(angle=30); //##jointC
bars_joint(angle=90); //##jointD
end_holder_90();// ##end
motor_holder_slim_90();// ##motor_slim
motor_holder_half_90();// ##motor_half

material_feeder();// ##feeder
//test();